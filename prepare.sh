#!/bin/bash

WG_DIR="/etc/wireguard"

if [ "${EUID}" -ne 0 ]; then
  echo "Please run as root"
  exit
elif ! test -f .env; then
  echo "Please create the .env file"
  exit
elif ! type wg; then
  echo "Please install WireGuard (wg)"
  exit
elif ! type wg-quick; then
  echo "Please install WireGuard (wg-quick)"
  exit
fi

test_variables() {
  VAR_NAMES="${1}"
  for name in ${VAR_NAMES}; do
    if ! test "$(grep ${name}= .env)"; then
      echo ${name} does not exist
      exit 1
    fi
  done
}

umask 077

if ! test -f "${WG_DIR}/privatekey"; then
  wg genkey | tee "${WG_DIR}/privatekey" | wg pubkey > "${WG_DIR}/publickey"
fi

echo "WireGuard server/client public key: $(cat "${WG_DIR}/publickey")"