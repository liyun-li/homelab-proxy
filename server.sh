#!/bin/bash

source prepare.sh
test_variables "PUBLIC_INTERFACE VPN_PORT VPN_SUBNET VPN_HOST CIDR VPN_CLIENT"
source .env

read PRIVATE_KEY < "${WG_DIR}/privatekey"
read -p "Client public key: " CLIENT_PUBLIC_KEY

cat > "${WG_DIR}/wg0.conf" << EOF
[Interface]
Address = ${VPN_HOST}/${CIDR}
ListenPort = ${VPN_PORT}
PrivateKey = ${PRIVATE_KEY}

[Peer]
PublicKey = ${CLIENT_PUBLIC_KEY}
AllowedIPs = ${VPN_CLIENT}/32
EOF

wg-quick down wg0 2>/dev/null
wg-quick up wg0
systemctl enable wg-quick@wg0

sysctl net.ipv4.ip_forward=1
sysctl -p

iptables --flush
iptables -t nat --flush

iptables -A FORWARD -i "${PUBLIC_INTERFACE}" -o wg0 -p tcp --syn --dport 80 -m conntrack --ctstate NEW -j ACCEPT
iptables -A FORWARD -i "${PUBLIC_INTERFACE}" -o wg0 -p tcp --syn --dport 443 -m conntrack --ctstate NEW -j ACCEPT
iptables -A FORWARD -i wg0 -o "${PUBLIC_INTERFACE}" -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -o wg0 -i "${PUBLIC_INTERFACE}" -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

iptables -t nat -A PREROUTING -i "${PUBLIC_INTERFACE}" -p tcp --dport 80 -j DNAT --to-destination "${VPN_CLIENT}"
iptables -t nat -A PREROUTING -i "${PUBLIC_INTERFACE}" -p tcp --dport 443 -j DNAT --to-destination "${VPN_CLIENT}"
iptables -t nat -A POSTROUTING -o wg0 -p tcp --dport 80 -d "${VPN_CLIENT}" -j SNAT --to-source "${VPN_HOST}"
iptables -t nat -A POSTROUTING -o wg0 -p tcp --dport 443 -d "${VPN_CLIENT}" -j SNAT --to-source "${VPN_HOST}"