# Homelab Proxy

Host your website without exposing your IP address to the Internet.

**Warning: This has only been tested on Ubuntu 20.04 LTS**

## Pre-requisite

You need a compute instance. I use DigitalOcean Droplets.

## Preparation

Ubuntu:

```bash
apt update
apt upgrade -y
apt install -y wireguard git
git clone https://gitlab.com/liyun-li/homelab-proxy
cd homelab-proxy
```

## Setup

1. Create a `.env` file in application root on the firewall server 

    ```bash
    PUBLIC_INTERFACE=eth0
    VPN_PORT=51820
    VPN_SUBNET=10.100.0.0
    VPN_HOST=10.100.0.1
    CIDR=29
    VPN_CLIENT=10.100.0.2
    ```

    _Note: Change `PUBLIC_INTERFACE` if necessary_

1. Create a `.env` file in application root on the private web server 

    ```bash
    VPN_PORT=51820
    VPN_SUBNET=10.100.0.0
    VPN_HOST=10.100.0.1
    CIDR=29
    VPN_CLIENT=10.100.0.2
    SERVER_PUBLIC_IP=SERVER_PUBLIC_IP
    ```

    _Note: Change `SERVER_PUBLIC_IP`._

1. Match the values for `VPN_PORT`, `VPN_SUBNET`, `VPN_HOST`, `CIDR` and `VPN_CLIENT` on the firewall server and the web server
1. Run `./server.sh` on server
1. Run `./client.sh` on client
1. Copy client public key to `server` input and wait until script finishes
1. Copy server public key to `client` input and wait until script finishes
1. Point your domain to the firewall's public IP

## TODO

* Containerize client & server
