#!/bin/bash

source prepare.sh
test_variables "VPN_SUBNET VPN_PORT VPN_HOST CIDR VPN_CLIENT SERVER_PUBLIC_IP"
source .env

read PRIVATE_KEY < "${WG_DIR}/privatekey"
read -p "Server public key: " SERVER_PUBLIC_KEY

cat > "${WG_DIR}/wg0.conf" << EOF
[Interface]
Address = ${VPN_CLIENT}/32
PrivateKey = ${PRIVATE_KEY}
PostUp = ping -c1 ${VPN_HOST}

[Peer]
PublicKey = ${SERVER_PUBLIC_KEY}
Endpoint = ${SERVER_PUBLIC_IP}:${VPN_PORT}
AllowedIPs = ${VPN_SUBNET}/${CIDR}
PersistentKeepalive = 30
EOF

wg-quick down wg0 2>/dev/null
wg-quick up wg0
systemctl enable wg-quick@wg0